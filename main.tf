provider "aws" {
  region     = var.region
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.secret_key
  version = "~> 2.0"
}

terraform {
  backend "http" {}
}
